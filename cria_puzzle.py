from pecas import id_peca, outro_lado, peca_simetrica, gira_peca, flip_peca, PERMITE_ENCAIXE_RETO_INTERNO
import random
import base64
import sys
from typing import List, Set
import bitmap_file
import shutil

"""

Cria quebra-cabeça no estilo do mostrado no vídeo
https://youtu.be/IH7pPoG-UfQ

Os do vídeo têm mais de uma solução.
Este tem três constantes de controle da geração:
PERMITE_ENCAIXE_RETO_INTERNO (pecas.py): se pode haver encaixes retos internos. Defini-los no main()
MAXIMO_SIMETRIAS_PERMITIDAS: controla o máximo de peças simétricas (aquelas que ao virar o lado são a mesma peça)
QTD_MAXIMA_SOLUCOES: Quantidade máxima de soluções. Zero para permitir qualquer quantidade.

A geração ocorre com força bruta até achar solução únida, no caso de não permitir repetida. Pode acontecer
de logo as primeiras peças serem intercambiáveis, e isso faz com que tivesse que esperar mudar a base das recursões
para achar algo. Portanto, se demorar mais que uns poucos minutos, o melhor a fazer é reiniciar. Colocando algumas
sessões em paralelo tende a achar algo rápido (lembrar de mudar o nome do arquivo de saída, passado como parâmetro).


cada peca é identificada na seguinte ordem
[esquerda, cima, direita, baixo]
0 é canto (sem encaixe), e 1 a 4 encaixe.
Par só encaixa com par, e ímpar com ímpar.
Menor que 3 só encaixa com maior ou igual a 3.
-1 é posição não definida.

Sintaxe:
python cria_puzzle.py nome_arquivo_saida.txt

Para rodar vários:
for %i in (1,2,3,4,5,6,7,8) do start cmd /c python cria_puzzle.py puzzle%i.txt
"""

# se maior que zero, a constante abaixo limita a quantidade de peças simétricas (que permanecem a mesma após flipar)
MAXIMO_SIMETRIAS_PERMITIDAS = 3
# Número máximo de soluções permitidas. Se 0, permite qualquer quantidade.
QTD_MAXIMA_SOLUCOES = 1


def pode_encaixar(tab, posicao, peca):
    # diz se a peça na orientação recebida pode entrar em tab na posição especificada, conferindo com a peça
    # à esquerda e com a peça acima, e vendo se é peça de borda em orientação correta.

    # conferir bordas da peça
    if posicao % 4 == 0:
        if peca[0] != 0:
            return False
    else:
        if peca[0] == 0 and not PERMITE_ENCAIXE_RETO_INTERNO:
            return False
    if posicao % 4 == 3:
        if peca[2] != 0:
            return False
    else:
        if peca[2] == 0 and not PERMITE_ENCAIXE_RETO_INTERNO:
            return False
    if posicao < 4:
        if peca[1] != 0:
            return False
    else:
        if peca[1] == 0 and not PERMITE_ENCAIXE_RETO_INTERNO:
            return False
    if posicao >= 12:
        if peca[3] != 0:
            return False
    else:
        if peca[3] == 0 and not PERMITE_ENCAIXE_RETO_INTERNO:
            return False

    # conferir com a peça acima
    if posicao >= 4:
        if tab[posicao - 4][3] != outro_lado(peca[1]):
            return False

    # conferir com a peça à esquerda
    if posicao % 4 != 0:
        if tab[posicao - 1][2] != outro_lado(peca[0]):
            return False

    # se passou em todas as conferencias é porque pode encaixar
    return True


def notacao_solucao(tab):
    # notacao com pecas em ordem normal, e direcoes em ordem também normal.
    notacao = ""
    for i in range(16):
        for j in range(4):
            notacao += str(tab[i][j])
    assert len(notacao) == 64
    return notacao


def solucao_simetrica(solucoes_encontradas, tab):
    """ Devolve se a nova solução encontrada é simétrica a alguma solução anterior.
    :param solucoes_encontradas: Conjunto de notações (ordem canônica) das soluções já encontradas.
    :param tab: Solução a verificar se é simétrica
    :return: Se a solução em tab é simétrica a alguma solução anterior.
    """
    notacao = ""
    ordem = (0, 1, 2, 3)  # posição de esquerda cima direita baixo em tab[i][0..3]
    for linha in range(4):
        for coluna in range(4):
            i = 4 * linha + coluna  # posição da peça em tab
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (1, 2, 3, 0)  # ordem rotacionando solução sentido horário
    for coluna in range(3, -1, -1):
        for linha in range(4):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (2, 3, 0, 1)  # ordem rotacionando solução duas vezes
    for linha in range(3, -1, -1):
        for coluna in range(3, -1, -1):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (3, 0, 1, 2)  # ordem rotacionando solução sentido anti-horário
    for coluna in range(4):
        for linha in range(3, -1, -1):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (2, 1, 0, 3)  # flipando na horizontal (eixo vertical)
    for linha in range(4):
        for coluna in range(3, -1, -1):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (0, 3, 2, 1)  # flipando na vertical (eixo horizontal)
    for linha in range(3, -1, -1):
        for coluna in range(4):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (3, 2, 1, 0)  # flipando na diagonal (eixo diagonal crescente)
    for coluna in range(3, -1, -1):
        for linha in range(3, -1, -1):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    notacao = ""
    ordem = (1, 0, 3, 2)  # flipando na diagonal (eixo diagonal decrescente)
    for coluna in range(4):
        for linha in range(4):
            i = 4 * linha + coluna
            for j in range(4):
                notacao += str(tab[i][ordem[j]])
    if notacao in solucoes_encontradas:
        return True

    return False


def conta_solucoes(tab, posicao, candidatos, qtd_solucoes, solucoes_encontradas, max_solucoes):
    # função recursiva para contagem de soluções, para ver se é até
    # max_solucoes. tab é preenchida com todos os candidatos,
    # que vieram originalmente de uma solução montada, que pode ou não ser até max_solucoes.
    if qtd_solucoes > max_solucoes:
        return qtd_solucoes
    if posicao == 16:  # se passou da posição final 15, todas peças puderam ser encaixadas
        # print(f"Achou solução {tab}")
        if qtd_solucoes == 0 or not solucao_simetrica(solucoes_encontradas, tab):
            solucoes_encontradas.add(notacao_solucao(tab))
            qtd_solucoes += 1
        return qtd_solucoes
    candidatos_originais = candidatos
    qtd_candidatos = len(candidatos_originais)
    candidatos = []  # trabalhar em cima de uma copia
    for i in range(qtd_candidatos):
        candidatos.append(candidatos_originais[i])
    for i_candidato in range(qtd_candidatos):  # tentar todos os candidatos em todos os giros (e flipando)
        candidato = list(candidatos[i_candidato])
        resto_candidatos = candidatos[0:i_candidato] + candidatos[i_candidato + 1:]
        for giros in range(8):  # oito posições incluindo a peça flipada
            if candidato == candidatos[i_candidato] and giros > 0:
                # se for peça simétrica com giro simples (exemplos: 3434, 1111), sair ao ficar igual ao giro 0.
                break
            if pode_encaixar(tab, posicao, candidato):
                tab[posicao] = candidato
                qtd_solucoes = conta_solucoes(tab, posicao + 1, resto_candidatos, qtd_solucoes, solucoes_encontradas,
                                              max_solucoes)
                tab[posicao] = -1
                if qtd_solucoes > max_solucoes:
                    return qtd_solucoes
            candidato = gira_peca(candidato)
            if giros == 3:  # flipar a peça após 4 giros. Se for peça simétrica (após flip), sair após o quarto giro.
                if peca_simetrica(candidato) or posicao == 0:
                    # não adianta flipar primeira peça, pois passaria somente por soluções simétricas com tudo virado.
                    break
                candidato = flip_peca(candidato)
    return qtd_solucoes


def tem_ate_n_solucoes(tab, max_solucoes):
    # diz se a solução com essas peças é única. tab deve estar totalmente preenchida com uma solução encontrada.
    tab_original = tab
    candidatos = []
    for i in range(16):
        candidatos.append(list(tab_original[i]))
    tab = [-1] * 16
    qtd_solucoes = 0
    # armazena notacao de uma solucao encontrada numa orientacao qualquer, para descastar as simetrias
    solucoes_encontradas = set()
    qtd_solucoes = conta_solucoes(tab, posicao=0, candidatos=candidatos, qtd_solucoes=qtd_solucoes,
                                  solucoes_encontradas=solucoes_encontradas, max_solucoes=max_solucoes)
    # print(f"Contagem de solucoes terminou em {qtd_solucoes}")
    if qtd_solucoes > max_solucoes:
        return False
    assert qtd_solucoes > 0
    return True


def cria_imagem_peca(lados_peca: str):
    # Lados peça é a descrição da peça na posição atual. Não é o id canônico da peça.
    # usadas coordenadas fixas. Um jeito bem mais curto, porém mais difícil de entender, seria controlar
    # os ângulos de desenho fazendo apenas uma rotina para os quatro lados.

    espaco_antes_quadrado = (3.0 / 8.0) * 50
    comprimento_quadrado = (1.0 / 4.0) * 50
    espaco_antes_triangulo = (1.0 / 3.0) * 50
    altura_triangulo = (1.0 / 3.0) * 50 * (3.0 ** 0.5) / 2.0
    bmp = bitmap_file.BitmapFile(medidas_em_mm=True, dimensoes=(90, 90), resolucao_dpi=200.0, inverter_eixo_y=False,
                                 deslocamento_janela=(-45, -45), espessura_linha=0.2)
    # linha superior
    if lados_peca[1] == '0':
        bmp.desenhar_linha(origem=(-25, 25), destino=(25, 25))
    elif lados_peca[1] == '1':  # quadrado externo
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25 + espaco_antes_quadrado, 25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, 25),
                           destino=(-25 + espaco_antes_quadrado, 25 + comprimento_quadrado))  # sobe
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, 25 + comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, 25 + comprimento_quadrado))  # direita
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, 25 + comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, 25))  # desce
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, 25),
                           destino=(25, 25))  # porção final
    elif lados_peca[1] == '3':  # quadrado interno
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25 + espaco_antes_quadrado, 25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, 25),
                           destino=(-25 + espaco_antes_quadrado, 25 - comprimento_quadrado))  # desce
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, 25 - comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, 25 - comprimento_quadrado))  # direita
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, 25 - comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, 25))  # sobe
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, 25),
                           destino=(25, 25))  # porção final
    elif lados_peca[1] == '2':  # triângulo externo
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25 + espaco_antes_triangulo, 25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_triangulo, 25),
                           destino=(0.0, 25 + altura_triangulo))  # sobe
        bmp.desenhar_linha(origem=(0.0, 25 + altura_triangulo),
                           destino=(25 - espaco_antes_triangulo, 25))  # desce
        bmp.desenhar_linha(origem=(25 - espaco_antes_triangulo, 25),
                           destino=(25, 25))  # porção final
    elif lados_peca[1] == '4':  # triângulo interno
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25 + espaco_antes_triangulo, 25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_triangulo, 25),
                           destino=(0.0, 25 - altura_triangulo))  # desce
        bmp.desenhar_linha(origem=(0.0, 25 - altura_triangulo),
                           destino=(25 - espaco_antes_triangulo, 25))  # sobe
        bmp.desenhar_linha(origem=(25 - espaco_antes_triangulo, 25),
                           destino=(25, 25))  # porção final

    # linha inferior
    if lados_peca[3] == '0':
        bmp.desenhar_linha(origem=(-25, -25), destino=(25, -25))
    elif lados_peca[3] == '1':  # quadrado externo
        bmp.desenhar_linha(origem=(-25, -25),
                           destino=(-25 + espaco_antes_quadrado, -25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, -25),
                           destino=(-25 + espaco_antes_quadrado, -25 - comprimento_quadrado))  # desce
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, -25 - comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, -25 - comprimento_quadrado))  # direita
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, -25 - comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, -25))  # sobe
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, -25),
                           destino=(25, -25))  # porção final
    elif lados_peca[3] == '3':  # quadrado interno
        bmp.desenhar_linha(origem=(-25, -25),
                           destino=(-25 + espaco_antes_quadrado, -25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, -25),
                           destino=(-25 + espaco_antes_quadrado, -25 + comprimento_quadrado))  # sobe
        bmp.desenhar_linha(origem=(-25 + espaco_antes_quadrado, -25 + comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, -25 + comprimento_quadrado))  # direita
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, -25 + comprimento_quadrado),
                           destino=(25 - espaco_antes_quadrado, -25))  # desce
        bmp.desenhar_linha(origem=(25 - espaco_antes_quadrado, -25),
                           destino=(25, -25))  # porção final
    elif lados_peca[3] == '2':  # triângulo externo
        bmp.desenhar_linha(origem=(-25, -25),
                           destino=(-25 + espaco_antes_triangulo, -25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_triangulo, -25),
                           destino=(0.0, -25 - altura_triangulo))  # desce
        bmp.desenhar_linha(origem=(0.0, -25 - altura_triangulo),
                           destino=(25 - espaco_antes_triangulo, -25))  # sobe
        bmp.desenhar_linha(origem=(25 - espaco_antes_triangulo, -25),
                           destino=(25, -25))  # porção final
    elif lados_peca[3] == '4':  # triângulo interno
        bmp.desenhar_linha(origem=(-25, -25),
                           destino=(-25 + espaco_antes_triangulo, -25))  # porção inicial
        bmp.desenhar_linha(origem=(-25 + espaco_antes_triangulo, -25),
                           destino=(0.0, -25 + altura_triangulo))  # sobe
        bmp.desenhar_linha(origem=(0.0, -25 + altura_triangulo),
                           destino=(25 - espaco_antes_triangulo, -25))  # desce
        bmp.desenhar_linha(origem=(25 - espaco_antes_triangulo, -25),
                           destino=(25, -25))  # porção final

    # linha direita
    if lados_peca[2] == '0':
        bmp.desenhar_linha(origem=(25, 25), destino=(25, -25))
    elif lados_peca[2] == '1':  # quadrado externo
        bmp.desenhar_linha(origem=(25, 25),
                           destino=(25, 25 - espaco_antes_quadrado))  # porção inicial
        bmp.desenhar_linha(origem=(25, 25 - espaco_antes_quadrado),
                           destino=(25 + comprimento_quadrado, 25 - espaco_antes_quadrado))  # direita
        bmp.desenhar_linha(origem=(25 + comprimento_quadrado, 25 - espaco_antes_quadrado),
                           destino=(25 + comprimento_quadrado, -25 + espaco_antes_quadrado))  # baixo
        bmp.desenhar_linha(origem=(25 + comprimento_quadrado, -25 + espaco_antes_quadrado),
                           destino=(25, -25 + espaco_antes_quadrado))  # esquerda
        bmp.desenhar_linha(origem=(25, -25 + espaco_antes_quadrado),
                           destino=(25, -25))  # porção final
    elif lados_peca[2] == '3':  # quadrado interno
        bmp.desenhar_linha(origem=(25, 25),
                           destino=(25, 25 - espaco_antes_quadrado))  # porção inicial
        bmp.desenhar_linha(origem=(25, 25 - espaco_antes_quadrado),
                           destino=(25 - comprimento_quadrado, 25 - espaco_antes_quadrado))  # esquerda
        bmp.desenhar_linha(origem=(25 - comprimento_quadrado, 25 - espaco_antes_quadrado),
                           destino=(25 - comprimento_quadrado, -25 + espaco_antes_quadrado))  # baixo
        bmp.desenhar_linha(origem=(25 - comprimento_quadrado, -25 + espaco_antes_quadrado),
                           destino=(25, -25 + espaco_antes_quadrado))  # direita
        bmp.desenhar_linha(origem=(25, -25 + espaco_antes_quadrado),
                           destino=(25, -25))  # porção final
    elif lados_peca[2] == '2':  # triângulo externo
        bmp.desenhar_linha(origem=(25, 25),
                           destino=(25, 25 - espaco_antes_triangulo))  # porção inicial
        bmp.desenhar_linha(origem=(25, 25 - espaco_antes_triangulo),
                           destino=(25 + altura_triangulo, 0.0))  # direita
        bmp.desenhar_linha(origem=(25 + altura_triangulo, 0.0),
                           destino=(25, -25 + espaco_antes_triangulo))  # esquerda
        bmp.desenhar_linha(origem=(25, -25 + espaco_antes_triangulo),
                           destino=(25, -25))  # porção final
    elif lados_peca[2] == '4':  # triângulo interno
        bmp.desenhar_linha(origem=(25, 25),
                           destino=(25, 25 - espaco_antes_triangulo))  # porção inicial
        bmp.desenhar_linha(origem=(25, 25 - espaco_antes_triangulo),
                           destino=(25 - altura_triangulo, 0.0))  # esquerda
        bmp.desenhar_linha(origem=(25 - altura_triangulo, 0.0),
                           destino=(25, -25 + espaco_antes_triangulo))  # direita
        bmp.desenhar_linha(origem=(25, -25 + espaco_antes_triangulo),
                           destino=(25, -25))  # porção final

    # linha esquerda
    if lados_peca[0] == '0':
        bmp.desenhar_linha(origem=(-25, 25), destino=(-25, -25))
    elif lados_peca[0] == '1':  # quadrado externo
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25, 25 - espaco_antes_quadrado))  # porção inicial
        bmp.desenhar_linha(origem=(-25, 25 - espaco_antes_quadrado),
                           destino=(-25 - comprimento_quadrado, 25 - espaco_antes_quadrado))  # esquerda
        bmp.desenhar_linha(origem=(-25 - comprimento_quadrado, 25 - espaco_antes_quadrado),
                           destino=(-25 - comprimento_quadrado, -25 + espaco_antes_quadrado))  # baixo
        bmp.desenhar_linha(origem=(-25 - comprimento_quadrado, -25 + espaco_antes_quadrado),
                           destino=(-25, -25 + espaco_antes_quadrado))  # direita
        bmp.desenhar_linha(origem=(-25, -25 + espaco_antes_quadrado),
                           destino=(-25, -25))  # porção final
    elif lados_peca[0] == '3':  # quadrado interno
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25, 25 - espaco_antes_quadrado))  # porção inicial
        bmp.desenhar_linha(origem=(-25, 25 - espaco_antes_quadrado),
                           destino=(-25 + comprimento_quadrado, 25 - espaco_antes_quadrado))  # direita
        bmp.desenhar_linha(origem=(-25 + comprimento_quadrado, 25 - espaco_antes_quadrado),
                           destino=(-25 + comprimento_quadrado, -25 + espaco_antes_quadrado))  # baixo
        bmp.desenhar_linha(origem=(-25 + comprimento_quadrado, -25 + espaco_antes_quadrado),
                           destino=(-25, -25 + espaco_antes_quadrado))  # esquerda
        bmp.desenhar_linha(origem=(-25, -25 + espaco_antes_quadrado),
                           destino=(-25, -25))  # porção final
    elif lados_peca[0] == '2':  # triângulo externo
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25, 25 - espaco_antes_triangulo))  # porção inicial
        bmp.desenhar_linha(origem=(-25, 25 - espaco_antes_triangulo),
                           destino=(-25 - altura_triangulo, 0.0))  # esquerda
        bmp.desenhar_linha(origem=(-25 - altura_triangulo, 0.0),
                           destino=(-25, -25 + espaco_antes_triangulo))  # direita
        bmp.desenhar_linha(origem=(-25, -25 + espaco_antes_triangulo),
                           destino=(-25, -25))  # porção final
    elif lados_peca[0] == '4':  # triângulo interno
        bmp.desenhar_linha(origem=(-25, 25),
                           destino=(-25, 25 - espaco_antes_triangulo))  # porção inicial
        bmp.desenhar_linha(origem=(-25, 25 - espaco_antes_triangulo),
                           destino=(-25 + altura_triangulo, 0.0))  # direita
        bmp.desenhar_linha(origem=(-25 + altura_triangulo, 0.0),
                           destino=(-25, -25 + espaco_antes_triangulo))  # esquerda
        bmp.desenhar_linha(origem=(-25, -25 + espaco_antes_triangulo),
                           destino=(-25, -25))  # porção final

    bmp.gravar_arquivo(f"{lados_peca}.bmp")


def cria_imagens(lista_pecas):
    # Cria imagem apenas se já não existir.
    for nome_peca in lista_pecas:
        try:
            f = open(f"./img/{nome_peca}.bmp", "rb")
            f.close()
        except FileNotFoundError:
            cria_imagem_peca(nome_peca)
            shutil.move(f"./{nome_peca}.bmp", f"./img/{nome_peca}.bmp")


def imprime_puzzle(tab):
    nome_arquivo = "puzzle.txt"
    if len(sys.argv) > 1:
        nome_arquivo = sys.argv[1]
    f = open(file=nome_arquivo, mode="w", encoding="UTF-8", newline="")
    lista_pecas = []
    for i in range(16):
        lista_pecas.append(id_peca(tab[i]))
    lista_pecas.sort()
    print(lista_pecas, file=f, end="\r\n")
    resposta = ""
    for i in range(16):
        e = " "
        if i % 4 == 3:
            e = "\r\n"
        resposta += str(tab[i]) + e
    resposta = base64.b64encode(resposta.encode('utf-8')).decode('utf-8')
    cont = 0
    for ch in resposta:
        print(ch, end="", file=f)
        cont += 1
        if cont == 72:
            cont = 0
            print("\r\n", end="", file=f)
    if cont != 0:
        print("\r\n", end="", file=f)
    f.close()
    print(f"Puzzle criado em {nome_arquivo}.")
    resp_usuario = '?'
    while resp_usuario == '?':
        resp_usuario = input("Deseja gerar as imagens das peças? ").strip().upper()
    if resp_usuario in ["S", "SIM", "Y", "YES", "SURE", "CLARO", "SIM!", "YES!"]:
        cria_imagens(lista_pecas)


def tenta_definir_peca(tab: List[List[int]], posicao: int, id_pecas_ja_adicionadas: Set, qtd_simetrias: int):
    tab_original = tab
    tab = []  # trabalhar em cima de uma copia
    for i in range(16):
        tab.append(list(tab_original[i]))
    if posicao == 16:
        if QTD_MAXIMA_SOLUCOES == 0 or tem_ate_n_solucoes(tab, QTD_MAXIMA_SOLUCOES):
            imprime_puzzle(tab)
            quit(0)
        else:
            # print("Achada solução não única, então continuando busca...")
            return
    ordem_lados = []
    qtd_tentativas_lado = [4] * 4
    # posições já definidas pelos encaixes de vizinhos não são alteradas. Somente as marcadas com -1.
    # abaixo uma ordem aleatória de tentativas de preenchimento
    for lado in range(4):
        if tab[posicao][lado] == -1:
            # quando o lado não está definido, tentará os 4 encaixes possíveis.
            ordem = [1, 2, 3, 4]
            random.shuffle(ordem)
            ordem_lados.append(ordem)
        else:
            # quando o lado já está definido, tem que entrar pelo menos uma vez com o valor original
            qtd_tentativas_lado[lado] = 1
            ordem_lados.append([tab[posicao][lado]])
    """ com a ordem aleatória sorteada, preencher todos os lados ainda não definidos (com os encaixes nos vizinhos,
    quando for o caso) e chamar recursivamente par a próxima peça.
    """
    for esquerda in range(qtd_tentativas_lado[0]):
        if tab_original[posicao][0] == -1:
            tab[posicao][0] = ordem_lados[0][esquerda]
            if posicao % 4 != 0:
                tab[posicao - 1][2] = outro_lado(tab[posicao][0])
        for cima in range(qtd_tentativas_lado[1]):
            if tab_original[posicao][1] == -1:
                tab[posicao][1] = ordem_lados[1][cima]
                if posicao >= 4:
                    tab[posicao - 4][3] = outro_lado(tab[posicao][1])
            for direita in range(qtd_tentativas_lado[2]):
                if tab_original[posicao][2] == -1:
                    tab[posicao][2] = ordem_lados[2][direita]
                    if posicao % 4 != 3:
                        tab[posicao + 1][0] = outro_lado(tab[posicao][2])
                for baixo in range(qtd_tentativas_lado[3]):
                    if tab_original[posicao][3] == -1:
                        tab[posicao][3] = ordem_lados[3][baixo]
                        if posicao < 12:
                            tab[posicao + 4][1] = outro_lado(tab[posicao][3])
                    ident = id_peca(tab[posicao])
                    if ident not in id_pecas_ja_adicionadas:
                        peca_formada_simetrica = False
                        if MAXIMO_SIMETRIAS_PERMITIDAS >= 0:
                            peca_formada_simetrica = peca_simetrica(tab[posicao])
                            if peca_formada_simetrica:
                                qtd_simetrias += 1
                        if qtd_simetrias < MAXIMO_SIMETRIAS_PERMITIDAS or MAXIMO_SIMETRIAS_PERMITIDAS < 0:
                            id_pecas_ja_adicionadas.add(ident)
                            tenta_definir_peca(tab, posicao + 1, id_pecas_ja_adicionadas, qtd_simetrias)
                            id_pecas_ja_adicionadas.remove(ident)
                        if peca_formada_simetrica:
                            qtd_simetrias -= 1

    # se chegar aqui é porque não tem puzzle possível com o que já tem na entrada
    if posicao == 0:
        raise RuntimeError("Impossível criar puzzle")


def main():
    tab = []
    for i in range(16):
        tab.append([-1] * 4)

    # marcar as posições de borda
    for i in range(4):
        tab[4 * i][0] = 0  # esquerda
        tab[i][1] = 0  # cima
        tab[4 * i + 3][2] = 0  # direita
        tab[15 - i][3] = 0  # baixo

    """ caso queira posições retas nas peças internas, definir arbitrariamente aqui, e alterar o
        valor da constante. Isso
        torna o puzzle mais difícil, pois as peças internas poderiam ficar na borda, ou seja, não
        se identifica facilmente quais as peças do centro.
    """
    if PERMITE_ENCAIXE_RETO_INTERNO:
        tab[5][3] = 0
        tab[9][1] = 0
        tab[5][2] = 0
        tab[6][0] = 0

    tenta_definir_peca(tab=tab, posicao=0, id_pecas_ja_adicionadas=set(), qtd_simetrias=0)


if __name__ == "__main__":
    main()
