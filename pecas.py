PERMITE_ENCAIXE_RETO_INTERNO = True

def outro_lado(tipo_encaixe):
    if tipo_encaixe == 0 and PERMITE_ENCAIXE_RETO_INTERNO:
        return 0
    assert 0 < tipo_encaixe < 5
    return ((tipo_encaixe - 1) + 2) % 4 + 1


def peca_simetrica(peca):
    for i in range(4):
        outro = []
        for j in range(4):
            outro.append(peca[(i + 4 - j) % 4])
        if peca == outro:
            return True
    return False


def gira_peca(peca):
    # gira a peça no sentido anti-horário
    p0 = peca[0]
    for i in range(3):
        peca[i] = peca[i + 1]
    peca[3] = p0
    return peca


def flip_peca(peca):
    # muda o lado da peça
    peca[1], peca[3] = peca[3], peca[1]
    return peca


def id_peca(peca):
    """ Devolve a representação canônica da peça, que consiste nos encaixes
    em ordem crescente. Implementado do jeito burro mesmo, por pura preguiça
    de pensar em algo mais eficiente, e porque é rápido mesmo do jeito que está.
    """
    menor = peca
    for i in range(4):
        novo = []
        for j in range(4):
            novo.append(peca[(i + j) % 4])
        if novo < menor:
            menor = novo
        novo = []
        for j in range(4):
            novo.append(peca[(i + 4 - j) % 4])
        if novo < menor:
            menor = novo
    ident = ""
    for i in range(4):
        ident += str(menor[i])
    return ident


def lista_pecas():
    todos = set()
    peca = [0] * 4
    for j in range(1, 5):
        peca[2] = j
        for k in range(1, 5):
            peca[3] = k
            ident = id_peca(peca)
            if ident not in todos:
                todos.add(ident)
    for i in range(1, 5):
        peca[1] = i
        for j in range(1, 5):
            peca[2] = j
            for k in range(1, 5):
                peca[3] = k
                ident = id_peca(peca)
                if ident not in todos:
                    todos.add(ident)
    for m in range(1, 5):
        peca[0] = m
        for i in range(1, 5):
            peca[1] = i
            for j in range(1, 5):
                peca[2] = j
                for k in range(1, 5):
                    peca[3] = k
                    ident = id_peca(peca)
                    if ident not in todos:
                        todos.add(ident)
    print(f"Total: {len(todos)}")
    print(sorted(list(todos)))


def main():
    lista_pecas()


if __name__ == "__main__":
    main()
